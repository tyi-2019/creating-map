import cv2 as cv
import numpy as np
import os
import subprocess
import glob
import extractor_function as extractor
import math
import camera_parameters as vars


def angle_to_metres(lon, lat):
    t = []
    # print("###", lon * 1000)
    t.append(lon * 111321 * math.cos(lat * math.pi / 180))
    t.append(lat * 111134)  # metres
    return t

def get_dots(x, y, roll, pitch):
    print("")

def spin_dot_around_center(x0, y0, x, y, angle):
    x1 = x - x0
    y1 = y - y0
    x2 = x1 * math.cos(angle / 180 * math.pi) - y1 * math.sin(angle / 180 * math.pi)
    y2 = x1 * math.sin(angle / 180 * math.pi) + y1 * math.cos(angle / 180 * math.pi)
    x2 += x0
    y2 += y0
    ans = [x2, y2]
    return ans

Photos = []
os.chdir('./images')
for file in glob.glob("*.JPG"):
    Photos.append(file)
Photos.sort()
os.chdir('../')


camera_feedback = extractor.extract("logs.txt", "mavlink_camera_feedback_t")
gps = extractor.extract("logs.txt", "mavlink_global_position_int_t")
# for i in range(0, len(gps)):
#     print(gps[i])
# exit()
# for i in range(0, 100):
#     print(camera_feedback[i]['data']['yaw'], camera_feedback[i]['data']['roll'], camera_feedback[i]['data']['img_idx'])


Background = cv.imread('background.png')
cv.imwrite("res.jpg", Background)

min_x = 1e9
min_y = 1e9
max_y = -1e9

for i in range(0, len(camera_feedback)):
    info = camera_feedback[i]
    x, y = info['data']['lng'], info['data']['lat']
    x, y = float(x.replace(",", ".")), float(y.replace(",", "."))
    x /= 1e7
    y /= 1e7
    t = angle_to_metres(x, y) # swap x and y
    x = t[0]
    y = t[1]
    min_x = min(min_x, x)
    min_y = min(min_y, y)
    max_y = max(max_y, y)

print("min, min", min_x, min_y)






##### TEST HOMOGRAPHY

# img = cv.imread('images/' + Photos[1].lower())

# points2 = np.array([
#     [100, 100],
#     [100, 500],
#     [500, 500], 
#     [500, 100]
# ])

# points1 = np.array([
#     [0, 0],
#     [0, img.shape[1]], 
#     [img.shape[0], img.shape[1]],
#     [img.shape[0], 0]
# ])

# h, status = cv.findHomography(points1, points2)
# Точки перечислять по часовой стрелке начиная с верхней правой
# fout = open('dots.txt','w')
# fout.write(str(points2[0][0]) + ' ' + str(points2[0][1]) + '\n')
# fout.write(str(points2[1][0]) + ' ' + str(points2[1][1]) + '\n')
# fout.write(str(points2[2][0]) + ' ' + str(points2[2][1]) + '\n')
# fout.write(str(points2[3][0]) + ' ' + str(points2[3][1]) + '\n')
# fout.close()

# args = ['./a.out', 'main.jpg', 'logo.jpg']
# subprocess.call(args) 

##### /TEST HOMOGRAPHY








for i in range(3, 50):
    photo = cv.imread('images/' + Photos[i].lower())
    info = camera_feedback[i]
    h = info['data']['alt_rel']
    h = h.replace(",", ".")
    h = abs(float(h))
    # x, y = info['data']['lng'], info['data']['lat']
    # print("ANGLE", x, y)
    # x, y = float(x.replace(",", ".")), float(y.replace(",", "."))
    x, y = get_gps_by_time(gps, info['time'])
    x /= 1e7
    y /= 1e7
    
    t = angle_to_metres(x, y)
    x = t[0]
    y = t[1]

    x -= min_x
    # y -= min_y
    y = max_y - y
    print("X AND Y:", x, y)
    dots = [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0]
    ]
    # ph_height_in_m = 0
    # ph_width_in_m = 0
    now_res = cv.imread("res.jpg")
    if (abs(float(info['data']['pitch'].replace(",", "."))) < 5 and abs(float(info['data']['roll'].replace(",", "."))) < 5):
        print("x and y in metres", x, y)
        print(info['data']['yaw'], info['data']['img_idx'])
        ph_height_in_m = 2 * h * math.tan(vars.vertical_angle / 360 * math.pi)
        ph_width_in_m = 2 * h * math.tan(vars.horizontal_angle / 360 * math.pi)
        dots[1] = [x - ph_width_in_m / 2, y + ph_height_in_m / 2]
        dots[2] = [x + ph_width_in_m / 2, y + ph_height_in_m / 2]
        dots[3] = [x + ph_width_in_m / 2, y - ph_height_in_m / 2]
        dots[0] = [x - ph_width_in_m / 2, y - ph_height_in_m / 2]


        zoom = 0.2
        ph_height_in_p, ph_width_in_p = photo.shape[0], photo.shape[1] # возможно, наоборот
        k = float((ph_width_in_m / ph_width_in_p) / zoom)
        x0, y0 = x * k, y * k
        fout = open("dots.txt", "w")
        for j in range(0, 4):
            dots[j][0], dots[j][1] = dots[j][0] * k, dots[j][1] * k 
            angle = float(info['data']['yaw'].replace(",", '.'))
            # angle = 0
            print("angle:", angle)
            dots[j] = spin_dot_around_center(x0, y0, dots[j][0], dots[j][1], angle)
            # dots[j][1] = Background.shape[1] - dots[j][1]
            # print(int(dots[j][0]), int(dots[j][1]))
            fout.write(str(int(dots[j][0])) + ' ' + str(int(dots[j][1])) + '\n')
            
        fout.close()
        args = ['./a.out', 'res.jpg', 'images/' + Photos[i].lower()]
        subprocess.call(args) 
        print("")
        print("")
        # res = cv.imread("res.jpg")
        # Background = res


    # print("")
    ##### ON MAP: 1 PIXEL - zoom METRES
    # zoom = 0.5
    # ph_height_in_p, ph_width_in_p = photo.shape[0], photo.shape[1] # возможно, наоборот
    # ph_h_metres = 2 * h * math.tan(vars.vertical_angle / 180 / 2 * math.pi)
    # ph_w_metres = 2 * h * math.tan(vars.horizontal_angle / 180 / 2 * math.pi)
    # k = float((ph_w_metres / photo_width) / zoom) 


    # print("@@@", ph_w_metres, (ph_w_metres / photo_width), k)
    # # print(x, y)
    # x = (int(x / zoom - min_x / zoom) - int(ph_h_metres / zoom / 2))
    # y = int(y / zoom - min_y / zoom) - int(ph_w_metres / zoom / 2)
    # print(x, y)
    # # print("")
    # photo = cv.resize(photo, (max(int(photo.shape[1] * k), 1), max(int(photo.shape[0] * k), 1)))
    # print("###", photo.shape[0], photo.shape[1])
    # Background = overflow(Background, photo, x, y, 45)
    # # x += photo.shape[0]
    # # cv.imwrite("resr.jpg", photo)

# cv.imwrite("res.jpg", Background)

