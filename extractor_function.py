def extract(file, param):
    inp = open(file, 'r').read()
    arr_str = inp.split("\n")
    arr_with_param = []
    for string in arr_str:
        if param in string:
            arr_with_param.append(string)
    out_arr = []
    for i in range(len(arr_with_param)):
        arr_with_param[i] = arr_with_param[i].replace("     ", ' ').replace("  ", ' ').split(' ')
        out_arr.append({})
        out_arr[i].update({'date': arr_with_param[i][0]})
        out_arr[i].update({'time': arr_with_param[i][1]})
        out_arr[i].update({'type_code': arr_with_param[i][9]})
        out_arr[i].update({'type': arr_with_param[i][10]})
        out_arr[i].update({'sig': ''})
        out_arr[i].update({'data': {}})
        if arr_with_param[i][-3] != "sig":
            out_arr[i]['sig'] = arr_with_param[i][-3]
        if param == "mavlink_statustext_t":
            text = ""
            try:
                while arr_with_param[i][15] != 'sig':
                    text += arr_with_param[i][14] + ' '
                    arr_with_param[i].pop(14)
                else:
                    text += arr_with_param[i][14]
                arr_with_param[i][14] = text
            except:
                pass
        for o in range(11, len(arr_with_param[i])-3-int(arr_with_param[i][4]), 2):
            out_arr[i]['data'].update({arr_with_param[i][o]: arr_with_param[i][o+1]})
    return out_arr
